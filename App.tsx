import {useState} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  ScrollView,
  FlatList,
  ToastAndroid,
} from 'react-native';
import TodoItem from './src/components/TodoItem';

export default function App() {
  const [inputText, setInputText] = useState('s');
  const [todos, setTodos] = useState<string[]>(['viec 1', 'viec 2']);

  function handleAddTodo() {
    if (inputText === '')
      return ToastAndroid.show(`Todo is empty`, ToastAndroid.SHORT);
    if (todos.includes(inputText))
      return ToastAndroid.show(
        `Todo ${inputText} is exited`,
        ToastAndroid.SHORT,
      );
    setTodos([...todos, inputText]);
  }

  function handleDeleteTodo(todo: string) {
    setTodos(todos.filter(t => t !== todo));
  }

  return (
    <View style={styles.container}>
      <View style={[styles.inputContainer]}>
        <TextInput
          style={styles.textInput}
          placeholder="Add todo"
          value={inputText}
          onChangeText={newTodo => setInputText(newTodo)}
        />
        <TouchableOpacity style={styles.button} onPress={handleAddTodo}>
          <Text style={styles.buttonText}>Add</Text>
        </TouchableOpacity>
      </View>
      <FlatList
        style={styles.itemList}
        data={todos}
        renderItem={todo => (
          <TodoItem
            name={todo.item}
            key={todo.item}
            handleDelete={handleDeleteTodo}
          />
        )}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 40,
  },
  inputContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  textInput: {
    flex: 1,
    borderWidth: 1,
    borderColor: '#cccccc',
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    borderRightWidth: 0,
    padding: 10,
  },
  button: {
    backgroundColor: 'blue',
    padding: 10,
    justifyContent: 'center',
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
  },
  buttonText: {
    color: 'white',
  },
  itemList: {},
});
