import Icon from 'react-native-vector-icons/FontAwesome';
import {View, Text, StyleSheet, Alert} from 'react-native';

export default function ({
  name,
  handleDelete,
}: {
  name: string;
  handleDelete: (todo: string) => any;
}) {
  function handleDeleteTodo() {
    Alert.alert(`Delete ${name} ?`, 'sure?', [
      {text: 'cancel'},
      {text: 'ok', onPress: () => handleDelete(name)}
    ])
  }
  return (
    <View style={styles.todoItem}>
      <Text style={styles.todoItemText}>{name}</Text>
      <Icon name="trash" size={25} onPress={handleDeleteTodo} />
    </View>
  );
}

const styles = StyleSheet.create({
  todoItem: {
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#dddddd',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  todoItemText: {},
});
